#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QMainWindow>

class RenderArea;
class GraphicsViewZoom;

namespace Ui {
class MainView;
}

class MainView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainView(QWidget *parent = nullptr);
    ~MainView();

private:
    Ui::MainView *ui;
    RenderArea* renderArea;
	GraphicsViewZoom* mZoomer;
};

#endif // MAINVIEW_H
