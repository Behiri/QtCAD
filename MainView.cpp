#include "MainView.h"
#include "ui_MainView.h"
#include <RenderArea.h>
#include <QGridLayout>
#include "Graphics_view_zoom.h"

MainView::MainView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainView)
{
    ui->setupUi(this);
    renderArea = new RenderArea;
	mZoomer = new GraphicsViewZoom(renderArea);
	mZoomer->setModifiers(Qt::NoModifier);
    renderArea->setWindowTitle("Test string");
    this->layout()->addWidget(renderArea);
}

MainView::~MainView()
{
    delete ui;
}
