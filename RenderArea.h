#ifndef RENDERAREA_H
#define RENDERAREA_H

//#include <QWidget>
#include <QGraphicsView>

class RenderArea : public QGraphicsView
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);

signals:
	void panned();


    // QWidget interface
public:
    QSize sizeHint() const override;
    QSize minimumSizeHint() const override;
	
private:
    QColor mBackgroundColor;
    QColor mShapeColor;

    // My variables
private:
    bool mPan;
    int mPanStartX;
    int mPanStartY;
	QPen mPen;
	QBrush mBrush;
    QGraphicsScene* mScene;
	int mSceneWidth;
	int mSceneHeight;
	int fGrid;
	int fScale;
	QPointF vOffset;
	QPointF vStartPosition;
	QPointF mCenter;

    // QWidget interface
public:
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;


protected slots:
	virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void wheelEvent(QWheelEvent* event) override;

protected:
	virtual void drawBackground(QPainter* painter, const QRectF& rect) override;


	virtual void showEvent(QShowEvent* event) override;

};

#endif // RENDERAREA_H
