#include "RenderArea.h"
#include <QPainter>
#include <QPaintEvent>
#include <QScrollBar>
#include <QGraphicsEllipseItem>

RenderArea::RenderArea(QWidget* parent) :
	QGraphicsView(parent),
	mBackgroundColor(QColor(0, 0, 64)),
	mShapeColor(QColor(255, 255, 0)),
	mPan(Qt::black),
	mBrush(Qt::blue, Qt::SolidPattern),
	mScene(new QGraphicsScene(-200, -200, 400, 400)),
	fGrid(10.0f), fScale(10.0f),
	vOffset{ 0.0f,0.0f },
	vStartPosition{ 0.0f, 0.0f },
	mCenter{ 0.0f,0.0f }
{
	//setDragMode(QGraphicsView::ScrollHandDrag);
	//this->mScene->setSceneRect(QRectF(QPointF(-1000, 1000), QSizeF(2000, 2000)));
    setScene(mScene);
	

	QPixmap pixmap(fGrid, fGrid);
	pixmap.fill(Qt::white);


	QPainter painter(&pixmap);
	painter.drawPoint(0, 0);
	painter.drawPoint(fGrid, fGrid);

	this->setBackgroundBrush(pixmap);

	horizontalScrollBar()->setStyleSheet("QScrollBar {height:0px;}");
	verticalScrollBar()->setStyleSheet("QScrollBar {width:0px;}");

	mCenter = mapToScene(viewport()->rect().center());
	vOffset = mCenter;
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(0, 0);
}

QSize RenderArea::sizeHint() const
{
    return QSize(600, 600);
}

void RenderArea::mousePressEvent(QMouseEvent *event)
{
    
    if(event->button() == Qt::RightButton)
    {
		mPan = true;
		vStartPosition = event->pos();
        mPanStartX = event->x();
        mPanStartY = event->y();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
        return;
    }
	else if (event->button() == Qt::LeftButton)
	{
		auto X = event->x();
		auto Y = event->y();

		auto remap = this->mapToScene(event->pos());

		

		auto item = mScene->addEllipse(remap.x(), remap.y(), 5, 5, mPen, mBrush);
		event->accept();
		return;
	}
    event->ignore();
}

void RenderArea::mouseReleaseEvent(QMouseEvent *event)
{

    if(event->button() == Qt::RightButton)
    {
        mPan = false;
        setCursor(Qt::ArrowCursor);
        event->accept();
        return;
    }
    event->ignore();
}

void RenderArea::mouseMoveEvent(QMouseEvent *event)
{
    if(mPan)
    {
		/*
		this->horizontalScrollBar()->setValue(this->horizontalScrollBar()->value() - (event->x() - mPanStartX));
        this->verticalScrollBar()->setValue(verticalScrollBar()->value() - (event->y() - mPanStartY));

		//auto xPos = this->horizontalScrollBar();
		//auto yPos = this->verticalScrollBar();

        //scrollContentsBy(mPanStartX - event->x(), mPanStartY - event->y());

        mPanStartX = event->x();
        mPanStartY = event->y();
        setCursor(Qt::ClosedHandCursor);
        event->accept();
		//emit panned();
        return;
		*/

		vOffset -= (event->pos() - vStartPosition);
		vStartPosition = event->pos();

		this->horizontalScrollBar()->setValue(vOffset.x());
		this->verticalScrollBar()->setValue(vOffset.y());
	}
	else
	{

	}
    event->ignore();
}

void RenderArea::keyPressEvent(QKeyEvent* event)
{
	if (event->key() == Qt::Key_Left)
		rotate(1);
	else if (event->KeyPress == Qt::Key_Right)
		rotate(-1);
}

void RenderArea::wheelEvent(QWheelEvent* event)
{
	/*if (event->delta() > 0)
		scale(1.25, 1.25);
	else
		scale(0.8, 0.8);*/
}

void RenderArea::drawBackground(QPainter* painter, const QRectF& rect)
{


	double x = rect.x();
	double y = rect.y();
	QRectF newRect(rect);
	mScene->addEllipse(mCenter.x(),mCenter.y(), 20,20, mPen, mBrush);
	newRect.setTopLeft(QPointF(floor(x / fGrid) * fGrid, floor(y / fGrid) * fGrid));
	painter->drawTiledPixmap(newRect, backgroundBrush().texture());

	mSceneHeight = mCenter.y();
	mSceneWidth = mCenter.x();
	mPen.setColor(Qt::green);
	mPen.setWidth(2);
	auto xAxis = mScene->addLine(-200, floor(mSceneHeight / 2), 200, floor(mSceneHeight / 2), mPen);
	mPen.setColor(Qt::red);
	auto yAxis = mScene->addLine(floor(mSceneWidth / 2), -200, floor(mSceneWidth / 2), 200, mPen);
	mPen.setColor(Qt::black);;
	xAxis->setZValue(-10);
	yAxis->setZValue(-10);
}

void RenderArea::showEvent(QShowEvent* event)
{
	

}






